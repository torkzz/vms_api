<?php
/**
 * Created by PhpStorm.
 * User: Vic Abalos
 * Date: 18/10/2017
 * Time: 9:58 AM
 */
include('lib/helper.php');
include('model.php');
//error_reporting(E_ALL);
//ini_set('display_errors', 1);
date_default_timezone_set('Asia/Manila');
class licence
{
    public $response;
    private $db_model;
    public $tracking_id;
    public $error;
    public $licence;
    public $TAG = "LICENCE";
    public $stringcount;
    public $status;    
//error_reporting(E_ALL);
//ini_set('display_errors', 1);
    public function licence()  {

        $this->tracking_id = time().rand();
        helper::create_logs("===== TRACKING ID $this->tracking_id =====");
        try {

            if (!helper::check_method('POST' ) ) {
                throw new Exception("109");
            }

           // if (!helper::check_auth( array( 'auth' => $_SERVER['PHP_AUTH_USER'], 'pass' => $_SERVER['PHP_AUTH_PW'] ) ) ){
           //      throw new Exception("111");
           //  }

            $this->db_model = new Model();
            $input = Helper::get_inputs();

            if (!$input->licence) {
                throw new Exception("103");
            }


 
            $this->licence = $this->db_model->check_licence( $input->licence );
            $this->status = false;

            if ($this->licence) {
                $this->status = $this->db_model->check_licence_status( $input->licence );
            }

            // helper::dd($this->licence);


            if (!$this->status) {
                if ($this->licence) {
                    $this->db_model->set_licence_to_already( $input->licence );
                }
            }


            $this->stringcount = strlen($input->licence);



        }catch ( Exception $e ){
            $this->error = $e->getMessage();
            $log = json_encode($this->error, JSON_UNESCAPED_SLASHES);
            helper::create_logs("$this->licence | API | $this->TAG | ERROR | $log ");
        }

        $data = array(
            'code' => helper::error_return($this->error),
            'message' => empty($this->error) ? 'Request Successful' : helper::get_error_message($this->error),
            'licence' => $this->licence ? true : false,
            'status' => (!$this->status) ? true : false,
            'count' => $this->stringcount
        );

        $message = $this->licence .' | API | '.$this->TAG ;
        helper::create_logs($message);
        $this->response = json_encode($data);
        return helper::http_response_code_header( ( empty($this->error ) ? '200' : '200' ) );

    }


}


header("Content-Type: application/json");
$data = new licence();
echo $data->response;