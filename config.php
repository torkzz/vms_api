<?php
/**
 * Created by PhpStorm.
 * User: Vic Abalos
 * Date: 12/10/2017
 * Time: 11:49 AM
 */

define('DEBUG', TRUE);
// local
define('HOST', 'localhost');
define('USER_NAME', 'root');
define('PASSWORD', '');
define('DATABASENAME', 'yondu_licence');

define('APP_VERSION', '1.5');
define('MARKET_VERSION', '1.5');