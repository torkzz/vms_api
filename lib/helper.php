<?php
/**
 * Created by PhpStorm.
 * User: Vic Abalos
 * Date: 18/10/2017
 * Time: 10:00 AM
 */
class Helper {

    static function check_auth($auth){
        if ( $auth['pass'] == 'q8K6dePbKBzWDthZ' && $auth['auth'] == 'tWw3Vw9E8Btpp3rH' ){
            return true;
        }
        return false;
    }

    static function get_error_message($message){
        $errorMessage = array(
            '101' => 'Already Subscribed',
            '102' => 'Msisdn Non-globe/TM' ,
            '103' => 'Invalid msisdn',
            '104' => 'Incomplete Input',
            '105' => 'Invalid Input' ,
            '106' => 'Db Connection Error' ,
            '107' => 'Verification Error' ,
            '108' => 'SMS Failed' ,
            '109' => 'Wrong Method' ,
            '111' => 'Auth Failed' ,
            '112' => 'Invalid Credentials or Deactivated' ,
            '113' => 'Profile Already Set' ,
            '114' => 'Password Criteria' ,
            '115' => 'Not Registered' ,
            '116' => 'Please Update!' ,
            '117' => 'Quiz Finish' ,
            '401' => 'Unauthorized' ,
        );
        return $errorMessage[$message];
    }

    static function http_response_code_header($code){
        header('X-PHP-Response-Code: '. $code , true, $code);
    }

    static function get_input()    {
        $json = file_get_contents('php://input');
        return $json ? json_decode($json) : false;
    }

    static function get_inputs()    {
        $json = $_POST;
        return $json ? json_decode(json_encode($json)) : false;
    }
    static function remove_numbers ($string){
        return preg_replace('/[0-9]+/', '', $string);
    }

    static function remove_letters ($string){
        return preg_replace('/[a-zA-Z]+/', '', $string);
    }

    static function remove_symbols ($string){
        return preg_replace('/[^\p{L}\p{N}\s]/u', '', $string);
    }

    static function check_globe_tm($msisdn){
        $prefix = substr($msisdn, 0, 3);
        if($prefix == '817' || $prefix == '905' || $prefix == '906' || $prefix == '915'
            || $prefix == '916' || $prefix == '917' || $prefix == '925'
            || $prefix == '926' || $prefix == '927' || $prefix == '935'
            || $prefix == '936' || $prefix == '937' || $prefix == '945'
            || $prefix == '973' || $prefix == '974' || $prefix == '975'
            || $prefix == '976' || $prefix == '977' || $prefix == '978'
            || $prefix == '979' || $prefix == '994' || $prefix == '995'
            || $prefix == '996' || $prefix == '997' || $prefix == '956' ){
            return true;
        }
        return false;
    }

    static function create_logs($log_array = array(), $filename = 'log')
    {

        if (empty($log_array)) {
            $log_array = array(
                'message' => 'log created'
            );
        }
        $path = "logs/";

        // filename
        $date = date("Ymd");
        $filename = $filename. "_" . $date. ".log";

        $logfile = 'logs/'.$filename;
        chmod($logfile, 0777);

        $logTxt = date("Y-m-d H:i:s")."\t". json_encode($log_array)."\n";

        if(file_exists($logfile)){
            file_put_contents($logfile, $logTxt, FILE_APPEND | LOCK_EX);
        }else{
            $myfile = fopen($logfile, "w") or die("Unable to open file!");
            fwrite($myfile, $logTxt);
            fclose($myfile);
        }
    }

    static function dd($s){
        var_dump($s);
        exit;
    }

    static function pp($s){
        print($s);
        exit;
    }

    static function generate_password($length = 6) {
//        $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    static function office_ip() {
        return ( $_SERVER['REMOTE_ADDR']  == "124.6.162.170" ) ? TRUE : FALSE;
    }

    static function check_allowed_number($msisdn){
        switch ($msisdn) {
            case '9178032215':
            case '9156445740':
            case '9055209652':
            case '9067138173':
                return TRUE;
                break;
            default:
                return TRUE;
                break;
        }
    }

    static function check_method($expected_method)
    {
        return ( strtoupper( $_SERVER['REQUEST_METHOD']) == $expected_method) ? TRUE : FALSE;
    }

    static function curl_sms($msisdn,$message,$rrn){
        $data = array(
            'SUB_Mobtel' =>$msisdn,
            'SMS_Message_String'=>$message,
            'SMS_SourceAddr'=>2910,
            'CSP_Txid'=>$rrn);
        //172.23.1.251 live
        //cbg-bellatrix.yondu.com staging/ locals
        $url = "http://".SMS_PERL."/web/api/2910/2910-receiver.php?".http_build_query($data);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FAILONERROR, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
    }

    static function compute_age($birth_day){
        $bday = new DateTime($birth_day);
        $today = new DateTime('now');
        $diff = $today->diff($bday);
//        printf('%d years, %d month, %d days', $diff->y, $diff->m, $diff->d);
        return $diff->y;
    }

    static function error_return($error){
        switch ($error) {
            case '101':
            case '102':
            case '103':
            case '104':
            case '105':
            case '106':
            case '107':
            case '108':
            case '109':
            case '111':
            case '112':
            case '113':
            case '114':
            case '115':
            case '116':
            case '117':
                return 400;
                break;
            case '403':
                return 403;
                break;
            case '':
                return 200;
                break;
            default:
                return $error;
                break;
        }
    }

}
