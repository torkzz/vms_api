/*
Navicat MySQL Data Transfer

Source Server         : localshit
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : yondu_licence

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2018-04-27 10:10:46
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tbl_licence
-- ----------------------------
DROP TABLE IF EXISTS `tbl_licence`;
CREATE TABLE `tbl_licence` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `licence_key` varchar(255) DEFAULT NULL,
  `project_id` int(11) NOT NULL,
  `status` varchar(255) DEFAULT '0',
  `latlong` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`,`project_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_licence
-- ----------------------------
INSERT INTO `tbl_licence` VALUES ('1', '11111111111111111111', '1', '1', null);
INSERT INTO `tbl_licence` VALUES ('2', '22222222222222222222', '1', '1', null);
INSERT INTO `tbl_licence` VALUES ('3', '33333333333333333333', '1', '1', null);
