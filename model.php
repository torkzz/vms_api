<?php
/**
 * Created by PhpStorm.
 * User: Vic Abalos
 * Date: 10/12/2017
 * Time: 9:04 AM
 */
require_once('lib/MysqliDb.php');
require_once('config.php');

error_reporting(E_ALL);
$data = array();
$db = new Mysqlidb (HOST, USER_NAME, PASSWORD, DATABASENAME);

class Model {
   


    function check_licence($licence)
    {
        global $db;
        $db->where ('licence_key', $licence);
        $db->get ('tbl_licence');
        if ($db->count > 0) {
            $db->disconnect();
            return true;
        }
        $db->disconnect();
        return false;
    }

    function check_licence_status($licence)
    {
        global $db;
        $db->where ('status', '1');
        $db->where ('licence_key', $licence);
        $status = $db->getOne ('tbl_licence','status');

        if ($db->count > 0) {
            $db->disconnect();
            return TRUE;
        }
        $db->disconnect();
        return FALSE;
    }

    function set_licence_to_already($licence)
    {
        global $db;
        if (!empty($licence) ){
            $db->where ('licence_key', $licence);
            $db->update ('tbl_licence', array( 'status' => 1 ) );
            $db->disconnect();
            return TRUE;
        }
        $db->disconnect();
        return FALSE;
    }


}


